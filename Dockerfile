FROM alpine
# py3-pygments is required for minted
# biber is required for biblatex (with biber back-end)
RUN apk add make texlive-full py3-pygments biber
